import tracery, { baseEngModifiers } from './tracery';
import seedrandom from 'seedrandom';

const enModifiers = {
  ...baseEngModifiers,
  that : function(s) {
    const str = s && s.trim();
    if (str[0] == 'a' && str[1] == ' ') {
      return "that " + str.substring(1)
    }
    if (str[0] == 'a' && str[1] == 'n' && str[2] == ' ') {
      return "this " + str.substring(2)
    }
    return str;
  }
}

const genres = [
  {
    name         : 'Point and click',
    desc         : [
      'a player is #input# to move #character# over the #world#',
    ],
    env          : ['scene', 'world', 'universe', 'house', 'city', 'country'],
    entity       : ['action list', 'NPC', 'key item', 'inventory', 'object in scene'],
    character    : ['the hero', 'the protagonist', 'Sam'],
    charInteract : ['look at', 'talk to', 'move to', 'pick', 'combine #entity# with'],
    input        : ['right clicking on #entity.a#', 'left clicking on #entity.a#', 'moving the mouse over #entity.a#', 'dragging #entity.a#'],
    operation    : ['combined with #entity.a#', 'removed', 'placed at #entity# location', 'used on #entity.a#'],
  },
  {
    name         : 'Choose your own adventure',
    desc         : [
      '#character# #charInteract# #entity.a# to learn new actions',
      '#character# uses #entity.s# to discover about the world',
      '#character# uses #entity.s# to interact with the world',
    ],
    env          : ['world', 'universe', 'text paragraph'],
    entity       : ['action list', 'NPC', 'key item', 'inventory', 'object in scene'],
    character    : ['the protagonist'],
    charInteract : ['look at', 'talk to', 'move to', 'pick', 'enter', 'get out of', 'combine #entity.a# with'],
    input        : ['typing action', 'typing cardinal direction', 'typing instructing'],
    operation    : ['created', 'combined with #entity.a#', 'removed', 'placed at #entity# location', 'used on #entity.a#'],
  },
  {
    name         : 'MOBA',
    desc         : [
      'the player is #input# to move #character# over the #world#',
      '#character# must #charInteract# #entity.a# to get #entity.a# #operation# #count#',
    ],
    env          : ['forest map', 'volcano map', 'antique map'],
    character    : ['the hero', 'a lengendary character', 'a greek divinity', 'an egyptian divinity'],
    entity       : ['creep', 'tower', 'mob', 'buff', 'map objective', 'other player', 'teammate', 'enemy', 'spell', 'potion', 'stuff'],
    charInteract : ['shoot at', 'cast damage spell at', 'cast healing spell at', 'cast protective spell at', 'move to'],
    input        : ['pressing "#entity#" key', 'right clicking on #world#', 'left clicking on #world#', 'right clicking #entity.a#', 'left clicking #entity.a#', 'moving the mouse'],
    operation    : ['spawned', 'destroyed', 'moved at #entity# location', 'damaged', 'buffed', 'healed'],
  },
  {
    name         : 'Puzzle',
    desc         : [
      'when #condition# #count#, then #entity.a# is #operation#',
      'if #character# #charInteract# #entity.a# #count#, then #entity.a# is #operation#',
    ],
    env          : ['board', 'grid', 'tilemap'],
    character    : ['the player'],
    entity       : ['gem', 'tile', 'grid', 'bonus', 'line', 'column', '#position# cell'],
    charInteract : ['swap #entity.a# place with', 'move #entity.a# toward', 'combine #entity.a# with', 'merge #entity.a# with'],
    input        : ['taping #entity.a#', 'swiping #entity.a#', 'touching screen', 'pressing button', 'dragging #entity.a#'],
    operation    : ['created', 'removed', 'placed at #entity# location', 'swapped with #entity.a#'],
  },
  {
    name         : 'Strategy',
    desc         : [
      '#character# #charInteract# #entity.a# to get #entity.a# #operation#',
      '#character# must get #entity.s# to have #entity.a# be #operation#',
    ],
    env          : ['map', 'skirmish map', 'team battle map'],
    character    : ['the commander', 'the main unit'],
    entity       : ['light unit', 'heavy unit', 'main resource', 'secondary resource', '#entity# generator'],
    charInteract : ['give instructions to', 'send retreat order to', 'send attack order to', 'ask protection for'],
    input        : ['pressing #entity# key', 'right clicking on #entity.a#', 'left clicking on #entity.a#', 'moving mouse over #entity.a#', 'selecting #entity.a#'],
    operation    : ['spawned', 'destroyed', 'moved away from #character#', 'moved close to #character#', 'damaged', 'shielded', 'repaired'],
  },
  {
    name         : 'Platformer',
    desc         : [
      '#character# #charInteract# #entity.a# to reach the end of the #world#',
      '#character# #charInteract# #entity.a# to save #character#',
    ],
    env          : ['side scrolling level', 'level', '3D world'],
    character    : ['the hero', 'an animal looking character', '"the captured friend to be saved"', 'the boss'],
    entity       : ['ground', '#position# platform', 'enemy', 'trap'],
    charInteract : ['jump on', 'run on', 'dash to', 'dash through', 'climb', 'ride'],
    input        : ['pressing button', 'pushing analog stick', 'smashing button #count#'],
    operation    : ['fallen', 'damaged', 'shielded', 'pushed up', 'pushed forward'],
  },
  {
    name         : 'Fighting',
    desc         : [
      '#character# must fight #character# with #character# without getting #operation#',
      '#character# must get #character# #operation#',
      '#character# #charInteract# #entity.a# and #charInteract# #entity.a# to survive',
    ],
    env          : ['arena', 'battle arena', 'level', 'stage'],
    character    : ['the player character', 'a fighter', 'the second player character', 'a martial art specialist', 'a monster', 'a creature'],
    entity       : ['AI controlled enemy', 'destructible decoration', 'weapon', 'shield', 'monster', 'creature'],
    charInteract : ['hit', 'get hit by', 'kick', 'protect #character# from', 'jump over', 'dodge'],
    input        : ['pressing button', 'pushing analog stick', 'smashing button #count#'],
    operation    : ['knocked out', 'damaged', 'shielded', 'healed'],
  },
  {
    name         : 'Sport',
    desc         : [
      '#character# faces #character# #count#. The goal is to #charInteract# #entity.a# to score',
      '#character# can #charInteract# #entity.a# to score, but when #condition# then #entity.a# is #operation#',
    ],
    env          : ['court', 'arena', 'field', 'ground'],
    character    : ['a team player', 'an opponent team player'],
    entity       : ['team', 'opposing team', 'goal', 'basket', 'ball'],
    charInteract : ['shoot #entity.a# towards', 'pass', 'run towards', 'hit', 'catch'],
    input        : ['pushing analog stick', 'pressing button', 'smashing button #count#'],
    operation    : ['excluded', 'scored', 'out', 'replaced'],
  },
  {
    name         : 'Management',
    desc         : [
      'the player is asked to #charInteract# #entity.a# by managing #character#',
      'by #input#, #character# can #charInteract# #entity.a#, in order to #charInteract# #entity.a#',
    ],
    env          : ['factory', 'laboratory', 'colony', 'city'],
    character    : ['a minion', 'a worker', 'the mayor', 'a scientist', 'a builder', 'a unit'],
    entity       : ['tool', 'machine', 'building', 'main resource', 'construction material'],
    charInteract : ['repair', 'connect #entity.a# with', 'build', 'destroy', 'collect'],
    input        : ['clicking #entity.a#', 'pressing #entity# key', 'right clicking on #entity.a#', 'left clicking on #entity.a#', 'moving mouse over #entity.a#', 'selecting #entity.a#'],
    operation    : ['repaired', 'built', 'destroyed', 'connected with #entity.a#'],
  },
  {
    name         : 'Shooter',
    desc         : [
      'will #hero# succeed not to get #operation# ? Right in #world.a#, #hero.that# must #charInteract# #entity.a# to battle against #character#'
    ],
    env          : ['war trenches', 'battlefield', 'devastated city', 'colonized planet'],
    character    : ['a soldier', 'a marine', 'the commander', 'a battleship', 'a tank'],
    entity       : ['gun', 'weapon', 'bullet', 'healthpack', 'ammo', 'enemy target', 'ally target', 'civilian'],
    charInteract : ['shoot', 'shoot #entity.s# at', 'fire', 'cover #character# from', 'fight #character# with'],
    input        : ['mass clicking', 'moving with keyboard', 'aiming at #entity.a#'],
    operation    : ['killed', 'destroyed', 'exploded', 'over', 'saved'],
  },
  {
    name         : 'Stealth',
    desc         : [
      'from the dark of #world.a#, #hero# is getting #character# #operation#. But for that, #hero.that# will need to #charInteract# #entity.a# #count#'
    ],
    env          : ['mansion', 'city by night', 'medieval city', 'bank facility', 'castle'],
    character    : ['a thief', 'an assassin', 'a victim', 'a target', 'the King', 'the Earl', 'the director'],
    entity       : ['dagger', 'blade', 'bag', 'sedative arrow', 'bow', 'treasure', 'money', 'lock', 'chest'],
    charInteract : ['unlock', 'use', 'hide from #character# using', 'rob', 'silently kill #character# with', 'break into #env# with', 'take #character# down using'],
    input        : ['pushing analog stick', 'slowly pressing button', 'slowly aiming with the mouse'],
    operation    : ['stolen', 'killed', 'assassinated', 'sedated', 'robbed', 'hidden', 'unlocked'],
  }, /*
   {
   name: 'Rhythm',
   desc: [

   ],
   env: [],
   character: [],
   entity: [],
   charInteract: [],
   input: [],
   operation: [],
   },
   {
   name: 'Metroidvania',
   desc: [

   ],
   env: [],
   character: [],
   entity: [],
   charInteract: [],
   input: [],
   operation: [],
   },
   {
   name: 'RPG',
   desc: [

   ],
   env: [],
   character: [],
   entity: [],
   charInteract: [],
   input: [],
   operation: [],
   },
   {
   name: 'Programming',
   desc: [

   ],
   env: [],
   character: [],
   entity: [],
   charInteract: [],
   input: [],
   operation: [],
   },
   {
   name: 'Card',
   desc: [

   ],
   env: [],
   character: [],
   entity: [],
   charInteract: [],
   input: [],
   operation: [],
   },
   {
   name: 'Iddle',
   desc: [

   ],
   env: [],
   character: [],
   entity: [],
   charInteract: [],
   input: [],
   operation: [],
   },
   {
   name: 'Detective',
   desc: [

   ],
   env: [],
   character: [],
   entity: [],
   charInteract: [],
   input: [],
   operation: [],
   }*/
]

// TODO add theme (location)

function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

export default function generate(seed = '') {
  let randFn = seedrandom(seed.toLowerCase());

  const shuffledGenres = shuffle(genres.slice(0), randFn);

  const genre1 = shuffledGenres.pop();
  const genre2 = shuffledGenres.pop();

  const grammar = generateMainGrammar(genre1, genre2, seed.toLowerCase())
  const text = grammar.flatten('"' + capitalizeFirstLetter(seed) + '" combines #origin#');

  const genre1TextualDescription = generateGenreGrammar(genre1, seed.toLowerCase() + '1')
    .flatten('Like in a #env# that implies #entity.s# and #entity.s#.');
  const genre2TextualDescription = generateGenreGrammar(genre2, seed.toLowerCase() + '2')
    .flatten('Mostly #entity.s# and #entity.s# in #env.a#.');

  return {
    genre1 : {
      name : genre1.name,
      desc : genre1TextualDescription,
    },
    genre2 : {
      name : genre2.name,
      desc : genre2TextualDescription,
    },
    text
  }
}

function generateMainGrammar(genre1, genre2, seed) {
  function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  tracery.setRng(seedrandom(seed));
  const grammar = tracery.createGrammar({
    'desc1'        : genre1.desc,
    'desc2'        : genre2.desc,
    'desc'         : [
      'when #condition#, then #character# #charInteract# #entity.a#',
      'when #condition# #count#, then #entity.a# is #operation#',
      'whenever #condition#, then #entity.a# is #operation# AND #entity.a# is #operation# ',
      'whenever #character# #charInteract# #entity.a#, then it is #operation#',
      'if #character# #charInteract# #entity.a# #count#, then #entity.a# is #operation#',
      'if #character# #charInteract# #entity.a#, #entity# is #operation#',
      'anytime #entity.a# #charInteract# #character#, then #character# is #operation#',
      'anytime #character# #charInteract# #entity.a#, then #character# #charInteract# #entity.a#',
      '#character# can #charInteract# #entity.a# and #charInteract# #entity.a#',
      '#character# can #charInteract# #entity.a#',
      '#character# can #charInteract# #position.a# #entity# in the #world#',
      '#character# #charInteract# #entity.a# in the #world#',
      '#character# #charInteract# #entity.a# and #charInteract# #entity.a#',
      '#character# #charInteract# #entity.s# to get #entity# #operation#',
      'the player must be #input# to #charInteract# #entity.a#',
      ...genre1.desc,
      ...genre2.desc],
    'env'          : [...genre1.env, ...genre2.env],
    'entity'       : [...genre1.entity, ...genre2.entity],
    'characters'   : [...genre1.character, ...genre2.character],
    'charInteract' : [...genre1.charInteract, ...genre2.charInteract],
    'operation'    : [...genre1.operation, ...genre2.operation],
    'condition'    : ['#entity.a# is #operation#', '#character# is #operation#', ...genre1.input, ...genre2.input],
    'input'        : [...genre1.input, ...genre2.input],
    'count'        : ['', 'a couple of time', 'twice', 'three times or more', 'at least four times', 'several times in a row'],
    'position'     : ['adjacent', 'diagonal', 'vertical', 'horizontal', 'nearby', 'distant'],
    'transition'   : ['Also', 'In Addition', 'Moreover', 'As well'],
    'place'        : [
      'in #world.a# that looks like #env.a#',
      'and take place in #world.a# representing #env.a#',
    ],

    'story'  : [`[entityGoal:#entity#][hero:#character#]${genre1.name} and ${genre2.name} genres #place#.
  In this game, #desc1#.
  #transition#, #desc2#.`],
    'origin' : ['#[world:#env#][entity:#entity#,#entity#,#entity#][character: #characters#, #characters#, #characters#]story#'],
  });
  grammar.addModifiers(enModifiers);
  grammar.distribution = 'shuffle';
  return grammar;
}

function generateGenreGrammar(genre, seed) {
  tracery.setRng(seedrandom(seed));
  const grammar = tracery.createGrammar({
    'entity'   : genre.entity,
    'env'      : genre.env,
    'origin'   : 'Like a #env# game with #entity.s# and #entity.s#',
    'count'    : ['a couple of time', 'twice', 'three times or more', 'at least four times', 'several times in a row'],
    'position' : ['', '', 'adjacent', 'diagonal', 'nearby', 'distant', 'moving', 'falling'],
  });
  grammar.addModifiers(enModifiers);
  grammar.distribution = 'shuffle';
  return grammar;
}

/**
 * Shuffles array in place. ES6 version
 * @param {Array} a items An array containing the items.
 */
function shuffle(a, rand) {
  for (let i = a.length - 1; i > 0; i--) {
    const j = Math.floor(rand() * (i + 1));
    [a[i], a[j]] = [a[j], a[i]];
  }
  return a;
}
