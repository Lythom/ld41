import React, { Component } from 'react';
import { Box, Flex } from 'grid-styled';
import styled, { ThemeProvider } from 'styled-components';

import generate from './generateCrossData'

import Genre from './Genre';

import './App.css';
import AnimatedDescBox from "./AnimatedDescBox";
import AnimatedLarge from "./AnimatedLarge";
import HashInput from "./HashInput";

const Button = styled.button`
  background: black;
  color: white;
  border: none;
  padding: 1rem 2rem;
  font-size: 1.5rem;
  cursor: pointer;
  font-family: Lato, sans-serif;
  font-weight: 900;
`

const NameLabel = styled.div`
  font-size: 1.2rem;
`

class App extends Component {

  state = {};

  constructor() {
    super();
  }

  componentDidMount() {
    this.regenerate();
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (window.location.hash !== '' && window.location.hash !== '#') {
      const {
              genre1,
              genre2,
              text
            } = generate(decodeURIComponent(window.location.hash.substr(1)));
      return {
        genre1,
        genre2,
        text : text.split('\n').map((val, i) => <span key={i}>{val}<br/></span>)
      }
    }
    return {
      genre1 : null,
      genre2 : null,
      text   : null,
    }
  }

  regenerate = (e) => {
    e && e.preventDefault();

    if (window.location.hash.substr(1) == '') {
      return this.setState({
        genre1 : null,
        genre2 : null,
        text   : null,
      });
    }
    const submission = decodeURIComponent(window.location.hash.substr(1));
    const {
            genre1,
            genre2,
            text
          } = generate(submission);

    if(window._paq) {
      window._paq.push(['trackEvent', 'Interaction', 'Generate', submission, text]);
    }

    this.setState({
      genre1 : null, genre2 : null, text : ''
    }, () => setTimeout(() => {
      this.setState({
        genre1,
        genre2,
        text : text.split('\n').map(val => <span>{val}<br/></span>)
      })
    }, 500))
    return false;
  }

  render() {
    return (
      <ThemeProvider
        theme={{
          space       : [0, 6, 12, 18, 24],
          breakpoints : ['64em', '128em', '256em']
        }}>
        <div className="App">
          <header className="App-header">
            <h1 className="App-title">Ultimate Game Jam Bravery</h1>
          </header>
          {this.state.text === null && (
            <Flex alignItems='center' justifyContent="center">
              <Box p={3} width={0.4}>
                <NameLabel>Are you brave enough ?
                  <ul style={{ textAlign : 'left' }}>
                    <ol>1. Name a game and generate some rules</ol>
                    <ol>2. Make the game</ol>
                  </ul>
                </NameLabel>
              </Box>
            </Flex>
          )}
          <Flex alignItems='center' justifyContent="center">
            <Box p={4} width={1 / 3}>
              <form action="#" onSubmit={this.regenerate}>
                <NameLabel>Name your game</NameLabel>
                <Box m={3}>
                  <HashInput/>
                </Box>
                <Button type="submit">I CAN DO IT !</Button>
              </form>
            </Box>
          </Flex>
          <Flex alignItems='center' justifyContent="center">
            <Box p={4} width={1 / 3}>
              <Genre x={300} genre={this.state.genre1}/>
            </Box>
            <Box width={0.1}>
              <AnimatedLarge pose={this.state.text ? 'visible' : 'hidden'}>×</AnimatedLarge>
            </Box>
            <Box p={4} width={1 / 3}>
              <Genre x={-300} genre={this.state.genre2}/>
            </Box>
          </Flex>
          <Flex alignItems='center' justifyContent="center" style={{ overflow : 'hidden' }}>
            <AnimatedDescBox pose={this.state.text ? 'visible' : 'hidden'}>
              {this.state.text}
            </AnimatedDescBox>
          </Flex>
          {this.state.text != null && (
            <Flex alignItems='center' justifyContent="center" style={{ overflow : 'hidden' }}>
              <Box p={3} width={1 / 2}>
                Make a game that respect those rules. <br/>Link "<a href={window.location.href}>{window.location.href}</a>" for ultimate glory !
              </Box>
            </Flex>
          )}

        </div>
      </ThemeProvider>
    );
  }
}

export default App;
