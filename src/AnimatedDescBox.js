import styled from 'styled-components';
import posed from "react-pose";

import {springTransition} from "./transitionProperties";

export default styled(posed.div({
  visible: {
    opacity: 1,
    y: 0,
    delay: 500,
    transition: springTransition,
  },
  hidden: {
    opacity: 0,
    y: -100,
  }
}))`
  z-index: -1;
  padding: 1em;
  max-width: 32em;
  font-size: 1.4rem;
  line-height: 1.4;
  text-align: left;
`