import {spring} from "popmotion";

export const springTransition = (props) => spring({
  ...props,
  stiffness: 500,
  damping: 21,
  duration: 1000
});
