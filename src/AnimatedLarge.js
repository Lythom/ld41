import styled from 'styled-components';
import posed from "react-pose";

import {springTransition} from "./transitionProperties";

export default styled(posed.div({
  visible: {opacity: 1, scale: 1, transition: springTransition,},
  hidden: {opacity: 0.2, scale: 0.2, transition: false}
}))`
  font-size: 7rem;
  line-height: 1.4;
  vertical-align: middle;
`