import React, {Component} from "react";

export default class hashInput extends Component {
  render() {
    return <input
      style={{fontSize: '1.2rem', lineHeight: 1.4}}
      value={decodeURIComponent(window.location.hash.substr(1))}
      onChange={e => {
        window.location.hash = encodeURIComponent(e.target.value);
        this.forceUpdate();
      }}
    />
  }
}