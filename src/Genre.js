import React from 'react'
import posed from 'react-pose';
import {tween, spring} from 'popmotion';
import {springTransition} from "./transitionProperties";

const Animatedbox = posed.div({
  visible: {
    opacity: 1,
    x: 0,
    transition: springTransition
  },
  hidden: {
    opacity: 0.2,
    x: props => props.x
  },
})

export default class Genre extends React.Component {

  render() {
    return <Animatedbox x={this.props.x} pose={this.props.genre == null ? 'hidden' : 'visible'}>
      <h2>{this.props.genre && this.props.genre.name}</h2>
      <em>{this.props.genre && this.props.genre.desc}</em>
    </Animatedbox>
  }
}